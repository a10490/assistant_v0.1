import speech_recognition as sr
from gtts import gTTS
import os
import webbrowser

# WIT_AI_KEY = 
r = sr.Recognizer()
#speech_recognition, google

def micro():
    with sr.Microphone() as source2:
        r.adjust_for_ambient_noise(source2, duration=0.3)
        audio = r.listen(source2)
        m_input = r.recognize_google(audio)
        m_input = m_input.lower()
        return m_input
def mowa(asystent):
    language = "en"
    pow = gTTS(text=asystent, lang=language, slow=False)
    pow.save("głos.mp3")
    os.system("mpg123 głos.mp3")

def read(text):
    language = 'en'
    pow = gTTS(text=text, lang=language, slow=False)
    pow.save("read.mp3")
    os.system("mpg123 read.mp3")
#===================================================================================

def menu():
    #menu główne
    us = 1
    while us != "goodbye":
        menu = ['search', 'sing', 'speak' ]
        asy = "choose search, sing, speak, goodbye"
        mowa(asy)
        us = micro()
        print("!!!!!!!!!!!!!!!!!!!wybrałeś: "+us+"    !!!!!!!!!!!!!!!!!!!!!!!!!!!")
        
        if us == menu[0]:
            mowa("choose what would you like to search")
            url = "https://duckduckgo.com/?t=ffab&q=" + micro() + "&ia=web"
            webbrowser.open_new(url)
                    
        elif us == menu[1]:
            mowa("lalalalalalaalaa")
                    
        elif us == menu[2]:
            mowa("Would you like to hear story?")
            print("yes/no")
            bajka = open("bajka.txt", "r")
            bajka = bajka.read()
            mowa(bajka)
            
        else: mowa("goodbye")

def news():
    url = 'https://wiadomosci.onet.pl/'
    webbrowser.open_new_tab(url)
    
#powitanie
asystent = "hello sir, i'm your assistant"
mowa(asystent)

#imię
mowa('what is your name')
us = micro()
mowa('beautiful name ' + us)
print("you told: "+us)

while us != 'goodbye':
    mowa("How can i help you")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!open menu, show some news!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    us = micro()

    if us == "open menu":
        menu()

    elif us == "show some news":
        news()

mowa('goodbye')

print(":)")
